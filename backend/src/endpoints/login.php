<?php

require_once "../libs/Bootstrap.php";

Bootstrap::init();

switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        $user = UserController::login();
        echo json_encode($user);
        break;
    case 'GET':
        $loginStatus = UserController::getLoginStatus();
        echo json_encode(['loginStatus' => $loginStatus]);
        break;
    case 'DELETE':
        UserController::logout();
        break;
    default:
        break;
}

