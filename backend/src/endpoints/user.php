<?php

require_once "../libs/Bootstrap.php";

Bootstrap::init();

switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        $user = UserController::post();
        echo json_encode($user);
        break;
    default:
        break;
}
