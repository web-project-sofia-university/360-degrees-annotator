<?php

require_once "../libs/Bootstrap.php";

Bootstrap::init();

switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        if(is_null($_FILES) || !array_key_exists('file', $_FILES)){
            throw new UploadException("Missing file");
            break;
        }
        $image = ImageController::post($_FILES['file']);
        break;
    case 'GET':
        $images = ImageController::get();
        echo json_encode($images);
        break;
    default:
        break;
}
