<?php

class NotLoggedException extends Exception {

    public function __construct($message) {
        parent::__construct($message);
    }
}
