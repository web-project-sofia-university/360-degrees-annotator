<?php

declare(strict_types=1);

class Image {
    private int $id;

    public function __construct (
        private string $fileName, 
        private string $uploadedOn,
        private int $uploadedBy,
        private string $name
    ) {}

    public function getId(): int {
        return $this->id;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getFileName(): string {
        return $this->filename;
    }

    public function getUploadedBy(): int {
        return $this->uploadedBy;
    }

    public function getUploadedOn(): string {
        return $this->uploadedOn;
    }

    public function setId(int $id): void {
        $this->id = $id;
    }
    
}
