<?php

declare(strict_types=1);

class User implements \JsonSerializable {
    private int $id;

    public function __construct (
        private string $username, 
        private string $password, 
        private string $email,
        private string $firstName,
        private string $lastName
    ) {}

    private static function hasNullField($jsonUser) {
        return is_null($jsonUser['username']) || is_null($jsonUser['password'])
                || is_null($jsonUser['email']) || is_null($jsonUser['firstName'])
                || is_null($jsonUser['lastName']);
    }

    public static function jsonToUser($jsonUser): User {
        if(User::hasNullField($jsonUser)) {
            $errMsg = "Register field is missing. "
                . "The required fields are: "
                . "username, password, firstName, lastName and email";

            throw new UserAlreadyExistsException($errMsg);
        }

        return new User($jsonUser['username'], $jsonUser['password'], $jsonUser['email'],
            $jsonUser['firstName'], $jsonUser['lastName']);
    }

    // added for current and future testing
    public static function Default(): User {
        return new User("test", "test", "test", "test", "test");
    }
    
    public function jsonSerialize() {
        $vars = get_object_vars($this);

        return $vars;
    }

    public function getUsername(): string{
        return $this->username;
    }

    public function getPassword(): string {
        return $this->password;
    }

    public function getEmail(): string {
        return $this->email;
    }

    public function getId(): int {
        return $this->id;
    }

    public function getFirstName(): string {
        return $this->firstName;
    }

    public function getLastName(): string {
        return $this->lastName;
    }

    public function setId(int $id): void {
        $this->id = $id;
    }

    public function getHashedPassword(): string {
        return password_hash($this->password, PASSWORD_DEFAULT);
    }

    public function validateFields(): void {
        if(is_null($this->username) || mb_strlen($this->username) < 1 || mb_strlen($this->username) > 255) {
            throw new InvalidUserDataException("Invalid username length");
        }

        if(is_null($this->password) || mb_strlen($this->password) < 8 || mb_strlen($this->password) > 200) {
            throw new InvalidUserDataException("Invalid password length");
        }

        if(is_null($this->email) || !filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidUserDataException("Invalid email format");
        }

        if(is_null($this->firstName) || mb_strlen($this->firstName) < 1 || mb_strlen($this->firstName) > 255) {
            throw new InvalidUserDataException("Invalid first name length");
        }

        if(is_null($this->lastName) || mb_strlen($this->lastName) < 1 || mb_strlen($this->lastName) > 255) {
            throw new InvalidUserDataException("Invalid last name length");
        }
    }

    public static function getUserFromRow($row): User {

        $user = new User($row["username"], $row["password"], $row["email"], $row["firstName"], $row["lastName"]);
        $user->setId(intval($row["id"]));

        return $user;
    }
}
