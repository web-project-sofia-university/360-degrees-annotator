<?php

class Config {
    public static function getDbConfig(): array {
        if(!file_exists(__DIR__ . "/appConfig.ini")) {
            throw new Exception("Missing config file");
        }
 
        return parse_ini_file(__DIR__ . '/appConfig.ini', true);
    }
}
