<?php

class UserRepository {
    public static function insert(User $user): User {
        $dbConnection = (new Db())->getConnection();
        $insertStatement = $dbConnection->prepare(
            "INSERT INTO user (username, password, email, firstName, lastName) 
             VALUES (:username, :password, :email, :firstName, :lastName);"
        );

        $insertSuccessful = $insertStatement->execute([
            'username' => $user->getUsername(),
            'password' => $user->getHashedPassword(),
            'email' => $user->getEmail(),
            'firstName' => $user->getFirstName(),
            'lastName' => $user->getLastName()
        ]);

        if ($insertSuccessful) {
            $user->setId($dbConnection->lastInsertId());
            return $user;
        } else {
            throw new RepositoryException($insertStatement->errorInfo()[2]);
        }
    }
    
    public static function check(string $username, string $password): ?User {

        $dbConnection = (new Db())->getConnection();
        $getStatement = $dbConnection->prepare(
            "SELECT * FROM user WHERE username = :username"
        );

        $getStatement->execute([
            "username" => $username,
        ]);

        $userData = $getStatement->fetch();

        if(!$userData || !password_verify($password, $userData["password"])) {
            throw new InvalidLoginDataException("Invalid username or password");
        }

        return User::getUserFromRow($userData);
    }

    public static function get(string $username): ?User {
        
        $dbConnection = (new Db())->getConnection();
        $getStatement = $dbConnection->prepare(
            "SELECT * FROM user WHERE username = :username;"
        );

        $getStatement->execute([
            "username" => $username
        ]);

        $userData = $getStatement->fetch();

        if($userData) {
            return User::getUserFromRow($userData);
        }

        return null;
    }
}
