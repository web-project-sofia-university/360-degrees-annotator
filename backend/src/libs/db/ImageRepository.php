<?php

class ImageRepository {
    public static function insert(string $fileName, int $userId, string $name): Image {
        $dbConnection = (new Db())->getConnection();
        $insertStatement = $dbConnection->prepare(
            "INSERT INTO images (fileName, uploadedOn, uploadedBy, name) 
             VALUES (:fileName, :uploadedOn, :uploadedBy, :name);"
        );

        $timestamp = date('Y-m-d H:i:s');
        $insertSuccessful = $insertStatement->execute([
            'fileName' => $fileName,
            'uploadedOn' => $timestamp,
            'uploadedBy' => $userId,
            'name' => $name
        ]);

        if (!$insertSuccessful) {
            throw new RepositoryException($insertStatement->errorInfo()[2]);
        }
        $image = new Image($fileName, $timestamp, $userId, $name);
        $image->setId($dbConnection->lastInsertId());
        return $image;
    }

    public static function get(int $userId): array {
        $dbConnection = (new Db())->getConnection();
        $selectStatement = $dbConnection->prepare(
            "SELECT * FROM images WHERE uploadedBy = :uploadedBy"
        );

        $selectStatement->execute([
            'uploadedBy' => $userId
        ]);

        $images = [];
        while ($image = $selectStatement->fetch()){
            $images[] = $image;
        }
        return $images;
    }
}
