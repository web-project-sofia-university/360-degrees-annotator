<?php

class Db {
    private $connection;

    public function __construct() {
        $dbConfig = Config::getDbConfig()["db"];

        $DB_HOST = $dbConfig["host"];
        $DB_NAME = $dbConfig["name"];
        $DB_USER_NAME = $dbConfig["username"];
        $DB_USER_PASSWORD = $dbConfig["password"];

        $this->connection = new PDO("mysql:host=$DB_HOST;dbname=$DB_NAME", $DB_USER_NAME, $DB_USER_PASSWORD,
            [
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            ]);
    }

    public function getConnection() {
        return $this->connection;
    }
}
