<?php

require_once __DIR__.'/../../constants/constants.php';

class ImageController {

    public static function post($file): Image {
      if(!UserController::getLoginStatus()) {
          throw new NotLoggedException("You must be logged to upload photos");
      }
      $targetDir = ROOT_PATH . "/360-degrees-annotator/uploaded/";
      $fileName = md5(microtime());
      $targetFilePath = $targetDir . $fileName;

      if(is_null($file)) {
          //this should not happen if data is sent through the ui
          throw new Error("empty file");
      }
      
      $allowTypes = array('jpg','png','jpeg','gif');
      $oldFileName = basename($file["name"]);
      $fileType = pathinfo($oldFileName,PATHINFO_EXTENSION);

      if(!in_array($fileType, $allowTypes)){
          throw new UploadException("Invalid file type. Only images are accepted");
      }
      if(!move_uploaded_file($file["tmp_name"], $targetFilePath)){
          throw new UploadException("File could not be uploaded at this time"); 
      }
      return ImageRepository::insert($fileName, $_SESSION['user_id'], $oldFileName);
    }

    public static function get(): array {
      if(!UserController::getLoginStatus()) {
          throw new NotLoggedException("You must be logged to view your uploaded photos");
      }
      $images = ImageRepository::get($_SESSION['user_id']);

      foreach ($images as &$img) {
        $img["path"] = PRJ_ROOT . $img["fileName"];
      }
      return $images;
    }
}
