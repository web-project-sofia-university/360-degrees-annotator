<?php

// enforces strict types for variables
declare(strict_types=1);

class UserController {


    public static function post(): User {
        $json = file_get_contents('php://input');
        $jsonUser = json_decode($json, true);

        $user = User::jsonToUser($jsonUser);

        $user->validateFields();

        if(!is_null(UserRepository::get($user->getUsername()))) {
            throw new UserAlreadyExistsException("Username is taken");
        }

        return UserRepository::insert($user);
    }

    public static function login(): User {
        $json = file_get_contents('php://input');
        $jsonUser = json_decode($json, true);

        $password = $jsonUser['password'];
        $username = $jsonUser['username'];

        if(is_null($username) || is_null($password)) {
            throw new InvalidUserDataException("Username or password is null");
        }

        $user = UserRepository::check($username, $password);

        session_start();
        $_SESSION['user_id'] = $user->getId();
        return $user;
    }

    public static function getLoginStatus(): bool {

        session_start();
        return isset($_SESSION['user_id']);
    }

    public static function logout(): void {

        session_start();
        session_destroy();
    }
}
