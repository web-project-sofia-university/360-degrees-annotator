<?php

/**
 * Class Bootstrap is used to import all other classes.
 * It also creates a global exception handler.
 */
class Bootstrap {

    public static function init(): void {
        self::registerExceptionHandlers();
        self::registerClassLoader();
        
    }

    private static function registerExceptionHandlers() {
        set_exception_handler(function ($e) {

            $errorCode = 500;

            if($e instanceof InvalidUserDataException) {
                $errorCode = 400;
            }

            if($e instanceof UserAlreadyExistsException) {
                $errorCode = 409;
            }
            
            if($e instanceof InvalidLoginDataException) {
                $errorCode = 404;
            }

            if($e instanceof UploadException) {
              $errorCode = 400;
            }

            if($e instanceof NotLoggedException) {
              $errorCode = 403;
            }

            $error = $e->getMessage();

            echo json_encode($error);
            http_response_code($errorCode);
        });
    }

    private static function registerClassLoader() {

        spl_autoload_register(function($className) {
            if (file_exists('../libs/' . $className . '.php')) {
                require_once '../libs/' . $className . '.php';
            } elseif (file_exists('../libs/db/' . $className . '.php')) {
                require_once '../libs/db/' . $className . '.php';
            } elseif (file_exists('../libs/exceptions/' . $className . '.php')) {
                require_once '../libs/exceptions/' . $className . '.php';
            } elseif (file_exists('../libs/controllers/' . $className . '.php')) {
                require_once '../libs/controllers/' . $className . '.php';
            } elseif (file_exists('../libs/model/' . $className . '.php')) {
                require_once '../libs/model/' . $className . '.php';
            } elseif (file_exists('../libs/config/' . $className . '.php')) {
                require_once '../libs/config/' . $className . '.php';
            }
        });
    }
}
