CREATE DATABASE IF NOT EXISTS `360_annotator` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `360_annotator`;

CREATE TABLE IF NOT EXISTS `user` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS `userStory` (
  `userId` int,
  `storyId` int,
  PRIMARY KEY (`userId`, `storyId`)
);

CREATE TABLE IF NOT EXISTS `story` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS `storyToScene` (
  `storyId` int,
  `sceneId` int,
  PRIMARY KEY (`storyId`, `sceneId`)
);

CREATE TABLE IF NOT EXISTS `scene` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS `marker` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `parentSceneId` int NOT NULL,
  `tooltip` varchar(255) NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `htmlContent` varchar(255),
  `nextSceneId` int
);

CREATE TABLE IF NOT EXISTS `images` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `fileName` varchar(255) NOT NULL,
  `uploadedOn` datetime NOT NULL,
  `uploadedBy` int NOT NULL,
  `name` varchar(255) NOT NULL
);

ALTER TABLE `userStory` ADD FOREIGN KEY (`userId`) REFERENCES `user` (`id`);

ALTER TABLE `userStory` ADD FOREIGN KEY (`storyId`) REFERENCES `story` (`id`);

ALTER TABLE `storyToScene` ADD FOREIGN KEY (`storyId`) REFERENCES `story` (`id`);

ALTER TABLE `storyToScene` ADD FOREIGN KEY (`sceneId`) REFERENCES `scene` (`id`);

ALTER TABLE `marker` ADD FOREIGN KEY (`parentSceneId`) REFERENCES `scene` (`id`);

ALTER TABLE `marker` ADD FOREIGN KEY (`nextSceneId`) REFERENCES `scene` (`id`);

ALTER TABLE `images` ADD FOREIGN KEY (`uploadedBy`) REFERENCES `user` (`id`);
