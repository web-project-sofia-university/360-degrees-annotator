export const backendURL = '/360-degrees-annotator/backend/src/endpoints';
export const fontAwesomeKit = 'https://kit.fontawesome.com/d4b6e59495.js';
export const frontendPath = '/360-degrees-annotator/frontend/src';
export const sidePanelID = 2;
