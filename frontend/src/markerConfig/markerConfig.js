import {sidePanelID} from '../constants/const.js';

function getMarkerConfigMenuHTML(markerTooltip,
    markerSceneName, markerContent) {
  return `
    <h1>Add new image</h1>
    <ul>
    <li class="markerLi">
      <input type="text" class="inputShape width100" id="updateMarkerNameInput"
          placeholder="Marker name" 
          value="${markerTooltip}">
    </li>
    <li class="markerLi">
      <input type="text" class="inputShape width100" 
          id="updateMarkerNextSceneInput"
          placeholder="Enter next scene name" value=${markerSceneName}>
    </li>
    <li class="markerLi">
      <button type="button" id="htmlTemplateButton" class="buttonShape buttonColor"> HTML Templates </button>
    </li>
    <li class="markerLi">
      <ul class="hidden" id="templates">
        <li class="htmlTemplateShape">
          <a class="templateLink" href="#">Person</a>
          <div class="templateContent hidden">
            <h1>Name</h1>
            <h2>Age</h2>
            <h3>Additional data</h3>
          </div>
        </li>
        <li class="htmlTemplateShape">
          <a class="templateLink" href="#">Building</a>
          <div class="templateContent hidden">
            <div>Description</div>
          </div>
        </li>
        <li class="htmlTemplateShape">
          <a class="templateLink" href="#">Animal</a>
          <div class="templateContent hidden">
            <h2>Animal type</h2>
            <h2>Animal size</h2>
            <h3>Additional data</h3>
          </div>
        </li>
        <li class="htmlTemplateShape">
          <a class="templateLink" href="#">Painting</a>
          <div class="templateContent hidden">
            <h1>Name</h1>
            <h2>Author</h2>
            <h2>Year</h2>
            <h3>Review</h3>
          </div>
        </li>
        <li class="htmlTemplateShape">
          <a class="templateLink" href="#">Landscape</a>
          <div class="templateContent hidden">
            <div>Description</div>
          </div>
        </li>
      </ul>
    </li>
    <li class="markerLi">
      <textarea 
          id="updateMarkerHTMLInput" 
          placeholder="Enter html text here."
          >${markerContent}</textarea>
    </li>
    <li class="markerLi">
      <button type="button width100" id="updateMarkerButton"
          class="buttonShape buttonColor">
        Configure marker
      </button>
    </li>
    </ul>
    `;
}

function updateMarker(e, viewer, story) {
  const marker = e.currentTarget.marker;
  const nextScene = document.querySelector('#updateMarkerNextSceneInput').value;
  const markerHTML = document.querySelector('#updateMarkerHTMLInput').value;
  const markerName = document.querySelector('#updateMarkerNameInput').value;

  const regex = /<\s*script/g;
  if (regex.test(markerHTML)) {
    viewer.notification.show({
      content: `No easy XSS here you bad boy!!!`,
      timeout: 3000,
    });
    return;
  }

  if (nextScene && markerHTML) {
    viewer.notification.show({
      content: `You must input only html or only a scene to redirect to!`,
      timeout: 3000,
    });
    return;
  }

  console.log(story.getCurrentScene().name);
  if (story.getCurrentScene().name === nextScene) {
    viewer.notification.show({
      content: `A marker can't redirect to the same scene`,
      timeout: 3000,
    });
    return;
  }

  if (markerHTML) {
    marker.config.content = markerHTML;
  }

  if (nextScene) {
    if (!story.hasScene(nextScene)) {
      viewer.notification.show({
        content: `No such scene exists`,
        timeout: 3000,
      });
      return;
    }

    marker.nextScene = nextScene;
  }

  marker.config.tooltip.content = markerName;
  story.updateMarker(marker);
  viewer.panel.hide(sidePanelID);
}

export function openMarkerMenu(marker, viewer, story) {
  const markerContent = marker.config.content ?
        marker.config.content : '';
  const markerNextScene = marker.nextScene ? marker.nextScene : '';

  viewer.panel.show({
    id: sidePanelID,
    width: '50%',
    content: getMarkerConfigMenuHTML(marker.config.tooltip.content,
        markerNextScene, markerContent),
  });

  toggleHtmlTemplates();
  fillTemplate();
  const updateButton = document.querySelector('#updateMarkerButton');
  updateButton.addEventListener('click', (e) => {
    updateMarker(e, viewer, story);
  }, false);
  updateButton.marker = marker;
}

function toggleHtmlTemplates() {
  const htmlTemplateButton = document.querySelector('#htmlTemplateButton');
  const templatesList = document.querySelector('#templates');
  htmlTemplateButton.addEventListener('click', (e) => {
    templatesList.classList.toggle('hidden');
  });
}

function fillTemplate() {
  const templates = document.querySelectorAll('.htmlTemplateShape');
  const textarea = document.querySelector('#updateMarkerHTMLInput');
  const templatesList = document.querySelector('#templates');
  for (let template of templates) {
    const link = template.querySelector('.templateLink');
    const content = template.querySelector('.templateContent');
    link.addEventListener('click', (e) => {
      textarea.value = content.innerHTML;
      templatesList.classList.toggle('hidden');
    });
  }
}
