import {importCSS, createElementFromHTML} from '../utils/importUtils.js';
import {frontendPath} from '../constants/const.js';

function getCreateStoryPopupFragmentHtml() {
  return `
      <div id="popup">
        <label>
            Enter story name:
        </label>
        <input class="popupInputShape" type="text" id="storyNameInput"/>
        <div id="inputValidationsText" class="hidden">
          Story name is required
        </div>
        <div id="buttonContainer">
          <button id="createButton" class="popupButtonShape popupButtonColor">
            Create
          </button>
          <button id="cancelButton" class="popupButtonShape popupButtonColor">
            Cancel
          </button>
        </div>
      </div>
  `;
}

function validateInput(storyName, input) {
  if (storyName == '') {
    input.classList.add('invalidInputHighlight');
    const errorMsg = document.querySelector('#inputValidationsText');
    errorMsg.classList.remove('hidden');
    return false;
  }
  return true;
}

export function openCreateFragmentPopup(createRow) {
  const popupHtml = getCreateStoryPopupFragmentHtml();
  const popup = createElementFromHTML(popupHtml);

  document.body.prepend(popup);
  const link = importCSS(frontendPath +
        '/createStoryInputPopup/popupFragment.css');

  const createButton = document.querySelector('#createButton');
  createButton.addEventListener('click', () => {
    const storyName = document.querySelector('#storyNameInput').value;
    const storyNameInput = document.querySelector('#storyNameInput');
    if (!validateInput(storyName, storyNameInput)) {
        return;
    }
    createRow(5, storyName);
    link.remove();
    popup.remove();
  });

  const cancelButton = document.querySelector('#cancelButton');
  cancelButton.addEventListener('click', () => {
    link.remove();
    popup.remove();
  });
}
