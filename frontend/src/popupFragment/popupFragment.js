import {importCSS, createElementFromHTML} from '../utils/importUtils.js';
import {frontendPath} from '../constants/const.js';

function getPopupFragmentHtml(text) {
  return `
      <div id="popup">
        ${text}
        <div id="buttonContainer">
          <button id="yesButton" class="popupButtonShape popupButtonColor">
            Yes!
          </button>
          <button id="noButton" class="popupButtonShape popupButtonColor">
            No!
          </button>
        </div>
      </div>
  `;
}


export function openYesNoPopup(text, onSuccess, onFail) {
  const popupHtml = getPopupFragmentHtml(text);
  const popup = createElementFromHTML(popupHtml);

  document.body.prepend(popup);
  const link = importCSS(frontendPath + '/popupFragment/popupFragment.css');

  const yesButton = document.querySelector('#yesButton');
  yesButton.addEventListener('click', onSuccess);
  yesButton.addEventListener('click', () => {
    link.remove();
    popup.remove();
  });

  const noButton = document.querySelector('#noButton');
  noButton.addEventListener('click', onFail);
  noButton.addEventListener('click', () => {
    link.remove();
    popup.remove();
  });
}
