export function importCSS(path) {
  // Create new link Element
  const link = document.createElement('link');
  // set the attributes for link element
  link.rel = 'stylesheet';
  link.type = 'text/css';
  link.href = path;

  document.head.appendChild(link);
  return link;
}

export function createElementFromHTML(htmlString) {
  const div = document.createElement('div');
  div.innerHTML = htmlString.trim();

  // Change this to div.childNodes to support multiple top-level nodes
  return div.firstChild;
}
