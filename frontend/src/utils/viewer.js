export function viewerConstructor() {
  const viewer = new PhotoSphereViewer.Viewer({
    container: document.querySelector('#viewer'),
    navbar: [
      'autorotate',
      'zoom',
      {
        id: 'help-button',
        content: 'Help',
        title: 'Help',
        className: 'help-button',
        onClick: () => {
          viewer.panel.show({
            id: 1,
            width: '50%',
            content: `
                <h1>HELP!!!</h1>
                <ul>
                  <li>Add tag: left click on image </li>
                  <li>
                    Make tag redirect to another image: <br>
                     right click tag and enter URL
                  </li>
                  <li>Remove tag: double click</li>
                </ul>
              `,
          });
        },
      },
      'caption',
      'fullscreen',
    ],
    plugins: [
      [
        PhotoSphereViewer.MarkersPlugin,
      ],
    ],
  });

  return viewer;
}
