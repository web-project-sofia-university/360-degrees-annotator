import {Scene} from './scene.js';
export class Story {
  constructor() {
    this.scenes = new Map();
    this.activeSceneName = '';
  }

  addScene(name, imageURL) {
    const newScene = new Scene(name, imageURL);
    this.scenes.set(name, newScene);
    this.activeSceneName = name;
  }

  hasScene(sceneName) {
    return this.scenes.has(sceneName);
  }

  updateMarker(marker) {
    this.scenes.get(this.activeSceneName).updateMarker(marker);
  }

  getCurrentScene() {
    return this.scenes.get(this.activeSceneName);
  }

  setActiveScene(sceneName) {
    this.activeSceneName = sceneName;
  }
}
