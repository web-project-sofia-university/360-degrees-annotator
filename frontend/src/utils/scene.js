export class Scene {
  constructor(name, imageURL) {
    this.imageURL = imageURL;
    this.markers = new Map();
    this.name = name;
  }

  updateMarker(marker) {
    const convertedMarker = this.convertLibMarkerToMarker(marker);
    this.markers.set(marker.config.id, convertedMarker);
  }

  convertLibMarkerToMarker(marker) {
    const nextScene = marker.nextScene ? marker.nextScene : '';
    return {
      id: marker.config.id,
      tooltip: marker.config.tooltip.content,
      longitude: marker.config.longitude,
      latitude: marker.config.latitude,
      nextScene: nextScene,
    };
  }
}
