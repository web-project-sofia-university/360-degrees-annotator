import {logout} from '../register/login.js';
import {importCSS, createElementFromHTML} from '../utils/importUtils.js';
import {frontendPath} from '../constants/const.js'

function getNavHTML(rootPath) {
  return `
    <nav>
      <ul id="navUl">
        <img src='${rootPath}/images/logoImage.png'
          alt='MemoryLane logo' id='navLogo'>
        <li class="navLi"><a href='${rootPath}/'>Home </a></li>
        <li class="navLi logged hidden"><a href='${rootPath}/myStories/myStories.html'>My stories</a></li>
        <li class="navLi logged hidden"><a href='${rootPath}/upload/upload.html'>Upload image</a></li>
        <li class="navLi not-logged hidden">
          <a href='${rootPath}/register/register.html'>Login</a>
        </li>
        <li class="navLi not-logged hidden">
          <a href='${rootPath}/register/register.html#register'>Register</a>
        </li>
        <li class="navLi logged hidden">
           <a id='logoutButton' href='#'>Logout</a>
        </li>
      </ul>
    </nav>
  `;
}

export function importNav() {
  const navRaw = getNavHTML(frontendPath);
  const nav = createElementFromHTML(navRaw);
  document.body.prepend(nav);

  importCSS(`${frontendPath}/nav/nav.css`);
  const logoutButton = document.getElementById('logoutButton');
  logoutButton.addEventListener('click', logout);
}

