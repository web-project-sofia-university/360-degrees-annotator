import {backendURL} from '../constants/const.js'
import {displayError} from '../messagePanels/displayMessage.js';
import {displaySuccess} from '../messagePanels/displayMessage.js';
import {importNav} from '../nav/nav.js';
import {checkLoginStatus} from '../register/login.js';

importNav();
checkLoginStatus();

function upload() {
  const input = document.getElementById('uploadedImage');
  var data = new FormData();
  data.append('file', input.files[0]);
  uploadImage(data);
}

function uploadImage(data) {
  fetch(backendURL + '/upload.php', {
    method: 'POST',
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache',
    body: data,
  }).then((response) => {
    console.log(response);
    if (!response.ok) {
      throw new Error(response.status);
    }
    return;
  })
      .then(handleUploadSuccessful)
      .catch(handleUploadFailed);
}

function handleUploadFailed(err) {
  if (err.message.includes('400')) {
    displayError('Only images can be uploaded here');
  } else if (err.message.includes('403')) {
    displayError('You must be logged in to upload photos');
  } else {
    displayError('Upload failed');
  }
}

function handleUploadSuccessful() {
  displaySuccess(`Operation successful!`)
}

const uploadImageButton = document.getElementById('uploadImageButton');
uploadImageButton.addEventListener('click', upload);
