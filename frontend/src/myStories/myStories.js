import '../nav/nav.js';
import {importNav} from '../nav/nav.js';
import {importFonts, addFontAwesome} from '../fonts/font.js';
import {checkLoginStatus} from '../register/login.js';
import {displaySuccess} from '../messagePanels/displayMessage.js';
import {frontendPath} from '../constants/const.js';
import {openCreateFragmentPopup} from
  '../createStoryInputPopup/popupFragment.js';

const fakeData = [
  {id: 1, name: 'Hitchikers'},
  {id: 2, name: 'Dune'},
  {id: 3, name: '1Q84'},
  {id: 4, name: 'Dark Tower'},
];

function createNewStory() {
  openCreateFragmentPopup(createRow);
}

function deleteElement(e) {
  const row = e.currentTarget.row;
  row.remove();
  // TODO: add backend calls
  displaySuccess('Story deleted');
}

function editElement(e) {
  const button = e.currentTarget;
  // TODO add logic to loadStory in editStory
  document.location.replace(frontendPath +
        `/editStory/editStory.html?storyid=${button.id}`);
}

function createRow(id, name) {
  const tr = document.createElement('tr');
  let td = document.createElement('td');
  td.innerText = id;
  tr.append(td);

  td = document.createElement('td');
  td.innerText = name;
  tr.append(td);

  td = document.createElement('td');
  td.classList.add('iconsTd');
  td.innerHTML = `
    <i class="fas fa-edit iconsShape"></i>
    <i class="fas fa-trash-alt iconsShape"></i>
  `;
  tr.append(td);
  const deleteButton = td.children[1];

  deleteButton.row = tr;
  deleteButton.name = name;
  deleteButton.id = id;
  deleteButton.addEventListener('click', deleteElement);

  const editButton = td.children[0];
  editButton.id = id;
  editButton.addEventListener('click', editElement);

  const tbody = document.querySelector('#storyContainer');
  tbody.append(tr);
}

function loadData() {
  for (const story of fakeData) {
    createRow(story.id, story.name);
  }
}

document.addEventListener('DOMContentLoaded', function() {
  importNav();
  importFonts();
  checkLoginStatus();
  addFontAwesome();
  loadData();
}, false);

const createStoryButton = document.querySelector('#createStoryButton');
createStoryButton.addEventListener('click', createNewStory);
