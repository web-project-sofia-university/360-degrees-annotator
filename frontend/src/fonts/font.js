import {createElementFromHTML} from '../utils/importUtils.js';
import {fontAwesomeKit} from '../constants/const.js';

function getFontLinks() {
  return `
    <div>
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@300&display=swap" rel="stylesheet">
    </div>
  `;
}


export function importFonts() {
  const fonts = createElementFromHTML(getFontLinks());
  document.head.append(fonts);
}

export function addFontAwesome() {
  const fontAwesomeScript = document.createElement('script');
  fontAwesomeScript.src = fontAwesomeKit;
  fontAwesomeScript.crossorigin = 'anonymous';

  document.head.append(fontAwesomeScript);
}
