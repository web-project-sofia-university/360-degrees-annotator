import {backendURL, frontendPath} from '../constants/const.js'
import {importCSS, createElementFromHTML} from '../utils/importUtils.js';
import {checkLoginStatus} from '../register/login.js';
import {importFonts, addFontAwesome} from '../fonts/font.js';

function getViewImagesPopupFragmentHtml() {
  return `
      <div id="popup">
        <ul id="imageList" class="logged hidden popupUl">
        </ul>
        <div class="not-logged hidden">
          You must be logged in to view this
        </div>
      </div>
  `;
}

function loadImages() {
  fetch(backendURL + '/upload.php', {
    method: 'GET',
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache',
  }).then(response => response.text().then(handleImages));
}

function handleImages(data) {
  const imageList = document.getElementById('imageList');
  imageList.innerHtml = "";
  let images = JSON.parse(data);
  for (const img of images) {
    imageList.appendChild(parseImage(img));
  }
}

function parseImage(img) {
  let listEl = document.createElement("li");
  listEl.classList.add("popupLi");
  let link = document.createElement("a");
  link.textContent = img.name;
  link.href = "#";

  link.addEventListener('click', () => {
    fillUrl(img.path);
    const cssLink = document.querySelector(".popupCssLink");
    cssLink.remove();
    const popup = document.getElementById("popup");
    popup.remove();
  });

  listEl.appendChild(link);
  let imgLink = document.createElement("a");
  let icon = document.createElement("i");
  icon.classList.add("far");
  icon.classList.add("fa-image");
  imgLink.appendChild(icon);
  imgLink.href = img.path;
  imgLink.target = "_blank";
  listEl.appendChild(imgLink);
  return listEl;
}

function createImagesPopup() {
  checkLoginStatus();
  importFonts();
  addFontAwesome();
  loadImages();
}

export function openViewImagesPopup() {
  const popupHtml = getViewImagesPopupFragmentHtml();
  const popup = createElementFromHTML(popupHtml);

  document.body.prepend(popup);
  const link = importCSS(frontendPath +
        '/viewImagesPopup/viewImagesPopup.css');
  link.classList.add("popupCssLink");
  createImagesPopup();
}

function fillUrl(imageUrl) {
  const sceneImgURLInput = document.querySelector('#sceneImgURL');
  sceneImgURLInput.value = imageUrl;
}
