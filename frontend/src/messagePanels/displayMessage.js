import {addFontAwesome} from '../fonts/font.js';

function displayMessage(msg, startIcon, boxColor, time) {
  const text = document.createTextNode(msg);
  const closeButton = document.createElement('button');
  const closeIcon = document.createElement('i');
  addFontAwesome();

  closeIcon.className = 'fas fa-times';

  closeButton.appendChild(closeIcon);
  closeButton.style = `
    background: none;
    outline: none;
    color: white;
    border: none;
    margin-left: auto;
  `;

  text.style = 'flex-grow: 2;';

  const errorContainer = document.createElement('span');

  errorContainer.style = `
    background: ${boxColor};
    position: absolute;
    top: 2%;
    right: 1%;
    padding: 10px;
    height: 30px;
    width: 300px;
    color: white;
    text-align: center;
    justify-content: flex-start;
    align-items: center;
    display: flex;
    border-radius: 5px;
    gap: 10px;
  `;


  closeButton.addEventListener('click', () => {
    errorContainer.remove();
  });


  errorContainer.appendChild(startIcon);
  errorContainer.appendChild(text);
  errorContainer.appendChild(closeButton);

  document.body.appendChild(errorContainer);

  time = time ? time : 4000;
  setTimeout(() => errorContainer.remove(), time);
}


export async function displayError(msg, time) {
  const alertIcon = document.createElement('i');
  alertIcon.className = 'fas fa-exclamation-triangle';
  displayMessage(msg, alertIcon, '#ca5e58', time);
}

export async function displaySuccess(msg, time) {
  const checkIcon = document.createElement('i');
  checkIcon.className = 'far fa-check-circle';
  displayMessage(msg, checkIcon, '#71b371', time);
}
