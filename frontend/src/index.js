import {importNav} from './nav/nav.js';
import {checkLoginStatus} from './register/login.js';
import {importFonts} from './fonts/font.js';

importNav();
importFonts();
checkLoginStatus();
