import {backendURL, frontendPath} from '../constants/const.js';
import {displaySuccess} from '../messagePanels/displayMessage.js';
import {displayError} from '../messagePanels/displayMessage.js';

export function sendLoginUserRequest(data) {
  fetch(backendURL + '/login.php', {
    method: 'POST',
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  }).then((response) => {
    if (!response.ok) {
      throw new Error(response.status);
    }
    return response.json();
  })
      .then(handleLoginSuccessful)
      .catch(handleLoginFailed);
}

function handleLoginFailed(err) {
  if (err.message.includes('404')) {
    displayError('Invalid username or password!');
  } else {
    displayError('Login failed');
  }
}

function handleLoginSuccessful() {
  displaySuccess(`Operation successful! You will be redirected shortly`)
      .then(() => document.location.replace(frontendPath + '/index.html'));
}

export function logout() {
  fetch(backendURL + '/login.php', {
    method: 'DELETE',
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache',
  }).then((response) => {
    if (!response.ok) {
      throw new Error();
    }
  })
      .then(handleLogoutSuccessful)
      .catch(handleLogoutFailed);
}

function handleLogoutSuccessful() {
  displaySuccess(`Operation successful! You will be redirected shortly`)
      .then(() => document.location.replace(frontendPath + '/index.html'));
}

function handleLogoutFailed() {
  displayError('Logout failed');
}

export function checkLoginStatus() {
  fetch(backendURL + '/login.php', {
    method: 'GET',
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache',
  }).then((response) => {
    if (!response.ok) {
      throw new Error();
    }
    return response.json();
  })
      .then(handleLoginStatusResponse)
      .catch((e) => {
        console.log('login status check failed: ', e);
      });
}

function handleLoginStatusResponse(loginStatusResponse) {
  if (loginStatusResponse.loginStatus) {
    // logged
    document.querySelectorAll('.logged')
        .forEach((element) => element.classList.remove('hidden'));
  } else {
    // not logged
    document.querySelectorAll('.not-logged')
        .forEach((element) => element.classList.remove('hidden'));
  }
}

