import {backendURL, frontendPath} from '../constants/const.js';
import {displayError} from '../messagePanels/displayMessage.js';
import {displaySuccess} from '../messagePanels/displayMessage.js';
import {sendLoginUserRequest} from './login.js';

function swapLoginAndRegisterAttributes(attribute1, attribute2, liDisplay) {
  const loginMenuButton = document.getElementById('loginMenuButton');
  const registerMenuButton = document.getElementById('registerMenuButton');

  loginMenuButton.classList.add(attribute1);
  loginMenuButton.classList.remove(attribute2);

  registerMenuButton.classList.add(attribute2);
  registerMenuButton.classList.remove(attribute1);

  const registerLis = document.getElementsByClassName('registerLi');

  for (const registerLi of registerLis) {
    registerLi.style.display = liDisplay;
  }
}

function openLoginMenu() {
  window.location.hash = '';
  const markedAttributes = 'headerButtonClicked';
  const unmarkedAttributes = 'headerButtonColor';

  swapLoginAndRegisterAttributes(markedAttributes, unmarkedAttributes, 'none');

  document.getElementById('loginHeader').style.display = 'block';
  document.getElementById('registerHeader').style.display = 'none';
  document.getElementById('loginSubmitButton').style.display = 'block';
}

function openRegisterMenu() {
  window.location.hash = '#register';
  const markedAttributes = 'headerButtonClicked';
  const unmarkedAttributes = 'headerButtonColor';

  swapLoginAndRegisterAttributes(unmarkedAttributes, markedAttributes,
      'list-item');

  document.getElementById('loginHeader').style.display = 'none';
  document.getElementById('registerHeader').style.display = 'block';
  document.getElementById('loginSubmitButton').style.display = 'none';
}

function highlightAndDisplayInputErrorText(input, invalidValidationTextDiv) {
  input.classList.add('invalidInputHighlight');
  invalidValidationTextDiv.style.display='inline';
}

function validateInput(input, failedValidation) {
  const validationTextDiv = input.nextSibling.nextSibling;
  validationTextDiv.style.display='none';

  if (failedValidation()) {
    highlightAndDisplayInputErrorText(input, validationTextDiv);
    return false;
  }

  return true;
}

function validRegisterFields(password, confirmPasswordInput, firstNameInput,
    lastNameInput, emailInput) {
  const emailRegex = /^\S+@\S+\.\S+$/;
  const email = emailInput.value;
  const firstName = firstNameInput.value;
  const lastName = lastNameInput.value;
  const confirmPassword = confirmPasswordInput.value;

  let formIsValid = true;
  formIsValid = validateInput(confirmPasswordInput, () =>
    confirmPassword != password || confirmPassword === '') && formIsValid;
  formIsValid = validateInput(emailInput,
      () => !emailRegex.test(email)) && formIsValid;
  formIsValid = validateInput(firstNameInput,
      () => firstName.length < 1 || firstName.length > 255) && formIsValid;
  formIsValid = validateInput(lastNameInput,
      () => lastName.length < 1 || lastName.length > 255) && formIsValid;

  return formIsValid;
}

function validLoginFields(usernameInput, passwordInput) {
  let formIsValid = true;

  const username = usernameInput.value;
  const password = passwordInput.value;

  formIsValid = validateInput(usernameInput,
      () => username.length < 1 || username.length > 255) && formIsValid;
  formIsValid = validateInput(passwordInput,
      () => password.length < 8 || password.length > 255) && formIsValid;

  return formIsValid;
}

function login() {
  const usernameInput = document.getElementById('username');
  const passwordInput = document.getElementById('password');

  validLoginFields(usernameInput, passwordInput);
  sendLoginUserRequest({
    username: usernameInput.value,
    password: passwordInput.value,
  });
}

function register() {
  const usernameInput = document.getElementById('username');
  const passwordInput = document.getElementById('password');
  const firstNameInput = document.getElementById('firstName');
  const lastNameInput = document.getElementById('lastName');
  const emailInput = document.getElementById('email');
  const confirmPasswordInput = document.getElementById('confirmPassword');


  const loginFormIsNotValid = !validLoginFields(usernameInput, passwordInput);
  const registerFormIsNotValid = !validRegisterFields(passwordInput.value,
      confirmPasswordInput, firstNameInput, lastNameInput, emailInput);
  if (loginFormIsNotValid || registerFormIsNotValid) {
    return;
  }

  sendRegisterUserRequest({
    username: usernameInput.value,
    password: passwordInput.value,
    confirmPassword: confirmPasswordInput.value,
    email: emailInput.value,
    firstName: firstNameInput.value,
    lastName: lastNameInput.value,
  });
}

function sendRegisterUserRequest(data) {
  fetch(backendURL + '/user.php', {
    method: 'POST',
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  }).then((response) => {
    if (!response.ok) {
      throw new Error(response.status);
    }
    return response.json();
  })
      .then(handleRegisterSuccessful)
      .catch(handleRegisterFailed);
}

function handleRegisterFailed(err) {
  if (err.message.includes('409')) {
    displayError('Username already exists!');
  } else {
    displayError('Register failed');
  }
}

function handleRegisterSuccessful() {
  displaySuccess(`Operation successful! You will be redirected shortly`)
      .then(() =>
        setTimeout(() => document.location.replace(frontendPath + '/register/register.html'), 3000));
}

function removeClass(event) {
  const target = event.target;
  target.classList.remove('invalidInputHighlight');
}

const loginMenuButton = document.getElementById('loginMenuButton');
const registerMenuButton = document.getElementById('registerMenuButton');

const loginSubmitButton = document.getElementById('loginSubmitButton');
const registerSubmitButton = document.getElementById('registerSubmitButton');

loginMenuButton.addEventListener('click', openLoginMenu);
registerMenuButton.addEventListener('click', openRegisterMenu);

loginSubmitButton.addEventListener('click', login);
registerSubmitButton.addEventListener('click', register);

const inputElements = document.getElementsByClassName('inputShape');
for (let i = 0; i < inputElements.length; i++) {
  inputElements[i].addEventListener('focus', removeClass);
}

const urlHash = window.location.hash;

if (urlHash === '#register') {
  openRegisterMenu();
}

