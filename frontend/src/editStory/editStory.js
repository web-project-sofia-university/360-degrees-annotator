import {importNav} from '../nav/nav.js';
import {viewerConstructor} from '../utils/viewer.js';
import {Story} from '../utils/story.js';
import {openMarkerMenu} from '../markerConfig/markerConfig.js';
import {checkLoginStatus} from '../register/login.js';
import {sidePanelID} from '../constants/const.js';
import {displayError} from '../messagePanels/displayMessage.js';
import {openYesNoPopup} from '../popupFragment/popupFragment.js';
import {openViewImagesPopup} from '../viewImagesPopup/viewImagesPopup.js';

importNav();
checkLoginStatus();

const createSceneButton =
    document.querySelector('button[id="createSceneButton"]');
const viewer = viewerConstructor();
const markersPlugin = viewer.getPlugin(PhotoSphereViewer.MarkersPlugin);
const story = new Story();

// function loadStory(story) {
//   story = story;
//   const activeScene = story.scenes[story.activeSceneId];
//   const activeSceneImgURL = activeScene.imageURL;
//
//   loadImage(activeSceneImgURL);
// }

function loadImage(imgURL) {
  viewer.panel.hide(sidePanelID);
  viewer.setPanorama(imgURL);
}

function addMarker(markerData) {
  const marker = markersPlugin.addMarker({
    id: markerData.id,
    longitude: markerData.longitude,
    latitude: markerData.latitude,
    image: 'https://photo-sphere-viewer.js.org/assets/pin-red.png',
    width: 32,
    height: 32,
    anchor: 'bottom center',
    tooltip: markerData.tooltip,
    data: {
      generated: true,
    },
  });

  marker.nextScene = markerData.nextScene;
  return marker;
}

function loadScene(sceneName) {
  markersPlugin.clearMarkers();
  story.setActiveScene(sceneName);
  const scene = story.getCurrentScene();
  loadImage(scene.imageURL);

  scene.markers
      .forEach((marker, _) => {
        console.log(marker);
        addMarker(marker);
      });
}

function loadSceneEvent(e) {
  const sceneName = e.currentTarget.id;
  loadScene(sceneName);
}

function addSceneButton(sceneName) {
  const button = document.querySelector(`#${sceneName}`);
  if (button) {
    return;
  }

  const newSceneButton = document.createElement('button');
  newSceneButton.classList.add('buttonShape');
  newSceneButton.classList.add('buttonColor');
  newSceneButton.classList.add('width30');
  newSceneButton.id = sceneName;
  newSceneButton.innerText = sceneName;
  newSceneButton.addEventListener('click', loadSceneEvent);

  const scenePickerDiv = document.querySelector('#scenePicker');
  scenePickerDiv.appendChild(newSceneButton);
}

function onMarkerSelect(_, marker, data) {
  if (viewer.panel) {
    viewer.panel.hide(sidePanelID);
  }

  if (marker.data && marker.data.generated) {
    if (data.dblclick) {
      markersPlugin.removeMarker(marker);
    } else if (data.rightclick) {
      openMarkerMenu(marker, viewer, story);
    } else {
      if (marker.nextScene) {
        loadScene(marker.nextScene);
      }
    }
  }
}

viewer.on('click', (_, data) => {
  if (!data.rightclick) {
    viewer.panel.hide(sidePanelID);
    data.id = '#' + Math.random();
    data.tooltip = 'Default marker tooltip';
    data.nextScene = '';

    const marker = addMarker(data);
    story.updateMarker(marker);
  }
});

markersPlugin.on('select-marker', (e, marker, data) => {
  onMarkerSelect(e, marker, data);
});

function validCreateSceneForm(sceneNameInput, sceneImgURLInput) {
  const sceneName = sceneNameInput.value;
  const sceneImgURL = sceneImgURLInput.value;

  sceneImgURLInput.classList.add('inputColor');
  sceneImgURLInput.classList.remove('invalidInputHighlight');

  sceneNameInput.classList.add('inputColor');
  sceneNameInput.classList.remove('invalidInputHighlight');

  let validForm = true;
  let errorMessage = '';
  if (!sceneImgURL) {
    sceneImgURLInput.classList.remove('inputColor');
    sceneImgURLInput.classList.add('invalidInputHighlight');
    validForm = false;
    errorMessage += 'Image URL is required. ';
  }

  if (!sceneName || sceneName.match(/^\d/)) {
    sceneNameInput.classList.remove('inputColor');
    sceneNameInput.classList.add('invalidInputHighlight');
    validForm = false;
    errorMessage += 'A name starting with a letter required.';
  }

  if (errorMessage) {
    displayError(errorMessage, 8000);
  }

  return validForm;
}

function createScene(sceneName, sceneImgURL) {
  story.addScene(sceneName, sceneImgURL);
  markersPlugin.clearMarkers();
  loadImage(sceneImgURL);
  addSceneButton(sceneName);
}

viewImagesButton.addEventListener('click', () => {
  openViewImagesPopup();
});

createSceneButton.addEventListener('click', () => {
  const sceneNameInput = document.querySelector('#sceneName');
  const sceneImgURLInput = document.querySelector('#sceneImgURL');
  const sceneName = sceneNameInput.value;
  const sceneImgURL = sceneImgURLInput.value;

  if (!validCreateSceneForm(sceneNameInput, sceneImgURLInput)) {
    return;
  }

  if (story.hasScene(sceneName)) {
    openYesNoPopup('You are overwriting an existing scene!' +
          ' Are you sure you want to do that', () => {
      createScene(sceneName, sceneImgURL);
    }, () => {
      return;
    });
    return;
  }
  createScene(sceneName, sceneImgURL);
});

createSceneButton.viewer = viewer;
